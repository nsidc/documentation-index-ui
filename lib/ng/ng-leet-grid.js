(function(){

  var leetGrid = angular.module('leetGrid', []);

  leetGrid.directive('leetGrid', function(){

    return {
      restrict: 'E',
      replace: true,
      scope: {
        options: '='
      },
      template:
        '<div style="text-align: center">' +
          '<div style="float: right;" class="leet-control leet-clickable" ng-click="addNewRecord()">' +
            'Add' +
          '</div>' +
          '<div style="clear: both;" />' +
          '<table class="{{ options.gridClass }}" ng-cloak>' +
            '<thead>' +
              '<th leet-th ng-repeat="column in options.columns" />' +
              '<th />' + // empty th, used for edit/save/delete controls
            '</thead>' +
            '<tbody>' +
              '<tr leet-tr ng-repeat="record in options.visibleRecords" />' +
            '</tbody>' +
          '</table>' +
        '</div>',
      link: function(scope, elem, attrs){
        console.log('linking leetgrid vvv');
        console.log(scope);

        // Respond when client resets records dynamically
        scope.$watch('options.records', function(newVal, oldVal){
          if(newVal != oldVal){
            console.log('refreshing view');
            scope.refreshView();
            console.log(scope);
          }
        });

        scope.refreshView = function(){
          scope.options.visibleRecords = scope.getVisibleRecords();
        };

        scope.getVisibleRecords = function(){
          return scope.options.records;
        };

        scope.addNewRecord = function(){
          // isEditing is used to determine whether input controls should display.
          var newRecord = { isEditing: true };

          // set default values, if any
          scope.options.columns.forEach(function(column){
            if(column.defaultValue !== undefined){
              newRecord[column.modelProp] = column.defaultValue;
            }
          });

          scope.options.records.unshift(newRecord);
        };

      }
    };

  });

  leetGrid.directive('leetTh', function(){

    return {
      restrict: 'A',
      replace: true,
      template:
        '<th>' +
          '<div>' +
            '{{ column.displayName }}' +
          '</div>' +
        '</th>',
      link: function(scope, elem, attrs){

        console.log('linking leetth');
        console.log(scope);

      }
    };

  });

  leetGrid.directive('leetTr', function(){

    return {
      restrict: 'A',
      replace: true,
      template:
        '<tr>' +
          '<td leet-td ng-repeat="column in options.columns" />' +
          '<td>' +
            '<span class="leet-control leet-clickable" ng-click="toggleRecordEditMode()">Edit </span>' +
            '<span class="leet-control leet-clickable" ng-click="saveRecord()">Save </span>' +
            '<span class="leet-control leet-clickable" ng-click="deleteRecord()">Delete </span>' +
          '</td>' +
        '</tr>',
      link: function(scope, elem, attrs){
        console.log('linking leettr');
        console.log(scope);

        scope.toggleRecordEditMode = function(){
          scope.record.isEditing = scope.record.isEditing === undefined ? true : !scope.record.isEditing;
        };

        // respond to isEditing state
        scope.refreshRecordView = function(){
          scope.record.contentTemplateClass = scope.record.isEditing === true ? 'leet-hidden' : '';
          scope.record.inputTemplateClass = scope.record.isEditing === true ? '' : 'leet-hidden';
        };

        scope.$watch('record.isEditing', function(newVal, oldVal){
          scope.refreshRecordView();
        });

        // call consumer defined fn to commit the record, provide
        // success and err callbacks.
        scope.saveRecord = function(){
          scope.options.saveRecord(
            scope.record,
            function(){ // success fn
              scope.record.isEditing = false;
              scope.rowEditStatusClass = '';
            },
            function(){ scope.rowEditStatusClass = 'leet-edit-error'; } // fail: add error css
          )
        };

        scope.deleteRecord = function(){
          scope.options.deleteRecord(
            scope.record,
            function(){  // success fn
              scope.options.records.splice(scope.options.records.indexOf(scope.record), 1);
            },
            function(){ 
              
            }
          )
        };

        // When the grid is initialized, input controls should be hidden.
        scope.record.inputTemplateClass = 'leet-hidden';
        scope.refreshRecordView();

      }
    };

  });

  leetGrid.directive('leetTd', ['$compile', function($compile){

    return {
      restrict: 'A',
      link: function(scope, elem, attrs){
        console.log('linking leettd');

        scope.leetTdKeypress = function($event){
          console.log($event);
        };

        var leetElem = $compile(getLeetTdWrapperTemplate(scope))(scope);
        elem.replaceWith(leetElem);

        console.log('leetElem is vvv');
        console.log(leetElem);

      }
    };

  }]);

  // helper functions
  function getLeetTdWrapperTemplate(scope){
    var template = scope.column.displayProp !== undefined ? 
      '{{ record[column.displayProp] }}' : '{{ record[column.modelProp] }}';

    return '<td>' +
        '<span class="{{ record.contentTemplateClass }}">' +
          template +
        '</span>' +
        getLeetTdInputTemplate(scope) +
      '</td>';
  }

  function getLeetTdInputTemplate(scope){
    var elemAttrs = 'ng-keypress="leetTdKeypress($event)" ' +
      'ng-model="record[column.modelProp]" class="leet-edit-content {{ record.inputTemplateClass }}"';
    
    return scope.editDataSource !== undefined ? 
      (
        '<select ' + elemAttrs + 
        ' ng-options="k as v for (k, v) in column.editDataSource" />'
      ) : scope.column.inputType == 'checkbox' ?
      (
        '<input ' + elemAttrs + ' type="checkbox" />'
      ) :
      '<input ' + elemAttrs + ' type="text" />';
  }

})();
