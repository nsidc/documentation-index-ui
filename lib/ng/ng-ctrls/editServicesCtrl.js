(function(){

  var app = angular.module('documentationApp');
  var documentationServiceUrl = 'http://documentation-index-vm.apps.int.nsidc.org:10680/';

  app.controller('editServicesCtrl', ['$scope', '$http', function($scope, $http){

    // When data comes back from the server, I want to update the view if any 
    // model properties have been changed or set.
    $scope.updateRecordProps = function(viewRecord, serverResponseRecord){
      viewRecord.id = serverResponseRecord.id;

      // update model props
      $scope.gridOptions.columns.forEach(function(column){
        viewRecord[column.modelProp] = serverResponseRecord[column.modelProp];
      });
    };

    $scope.gridOptions = {
      columns: [
        { displayName: 'Name', modelProp: 'name' },
        { displayName: 'Base endpoint', modelProp: 'endpoint' },
        { displayName: 'Documentation endpoint', modelProp: 'documentation_endpoint' },
        { displayName: 'Advertised', modelProp: 'is_advertised', inputType: 'checkbox', defaultValue: false }
      ],
      saveRecord: function(record, successFn, errorFn){
        // pre-existing records will have an id, so the op is an update
        if(record.id !== undefined){
          $http.put(documentationServiceUrl + 'protected/service/' + record.id, record)
            .success(successFn)
            .error(errorFn);
        }
        else{
          $http.post(documentationServiceUrl + 'protected/service', record)
            .success(function(responseRecord){
              $scope.updateRecordProps(record, responseRecord);
              successFn();
            })
            .error(errorFn);
        }
      },
      deleteRecord: function(record, successFn, errorFn){
        if(record.id === undefined){
          successFn();
        }
        else{
          $http.delete(documentationServiceUrl + 'protected/service/' + record.id)
            .success(successFn)
            .error(errorFn);
        }
      },
      gridClass: 'table table-striped table-hover',
      records: []
    };

    $http.get(documentationServiceUrl).success(function(response){
      console.log(response);
      $scope.gridOptions.records = response;
    });

  }]);

})();
