# Documentation Index UI

This is the front end for the documentation index, currently using Swagger.

This project uses node and express to serve static assets.  The correct service
endpoint for retrieving the list of documented services will be added to the
html documents as part of a jenkins provision script, which is defined in the
documentation-index-vm project.

## Service owner info

### Adding your service to the tool

This tool exists in 4 environments.  You must follow the steps for each environment you want your service documentation to be visible in.

Environments:

* Integration: http://integration.web-services-docs.apps.int.nsidc.org/
* QA: http://qa.web-services-docs.apps.int.nsidc.org/
* Staging: http://staging.web-services-docs.apps.int.nsidc.org/
* Prod: http://web-services-docs.apps.int.nsidc.org/

Steps to include your documentation:

* Update your service to expose a swagger service documentation json configuration object that follows the swagger spec
    * Example: http://nsidc.org/api/dataset/2/SwaggerDocs (This example is not intended to be a comprehensive example of all possible configuration options of the JSON object)
    * For more information about the swagger spec, please see https://github.com/wordnik/swagger-spec/blob/master/versions/1.2.md
    * Note: The Swagger UI does not necessarily display information related to every option or key that is defined in the swagger spec
* Visit the edit-services.html page at e.g. http://web-services-docs.apps.int.nsidc.org/edit-services.html
* Add information about your service (the name and endpoint for the swagger configuration object), and then save it
* Refresh the swagger UI.  You should see your service documentation with the ability to try out example API calls.

Troubleshooting:

* If your service documentation cannot be expanded, then there was a problem trying to read the swagger JSON object from your service.
    * Open the network panel in your web browser and verify that the swagger JSON object endpoint is accessible.
    * Verify that cross origin requests are enabled in your service.  Swagger uses Ajax and CORS to pull in the JSON configuration objects
    * Check the swagger spec at https://github.com/wordnik/swagger-spec/blob/master/versions/1.2.md and ensure that all required key/value pairs are defined in your configuration object

## Developer info

### Dependencies

* This project pulls data from several sources to create the interactive Swagger UI.
    * A data store which defines each service that exposes documentation : https://bitbucket.org/nsidc/documentation-index/overview 
    * Each service that is defined by the data store will expose a configuration object that has information about each API call

## Get and build

```bash
git clone git@bitbucket.org:nsidc/documentation-index-ui.git
cd ./documentation-index-ui
npm install
```

## Run

```bash
node ./server.js
```

## Test

Run browser tests with:

```bash
NODE_ENV=development grunt casperjs
```

Browser tests can be added by modifying
`test/casperjs/documentation-index_tests.js`, or creating a new file in
`test/casperjs/`.

* if the environment variable `NODE_ENV` is set to "integration", "qa",
  "staging", or "production", the CasperJS tests will run against the app
  actually running on the given environment
* if `NODE_ENV` has any other value, the tests will run against the app running
  at `localhost:8081`
* if running the tests against the local app, be sure the `backendServiceUrl` in
  `dist/index.html` is set appropriately
    * if [the backend project](https://bitbucket.org/nsidc/documentation-index)
      is running locally on the same VM as the the UI, `backendServiceUrl`
      should be set to something like `http://localhost:3000/api-docs`, using
      the port the backend service is running on (`3000`), rather than the host
      port that is forwarded to that port on the VM (`13000`, as in the
      [Vagrantfile in the development VM project](https://bitbucket.org/nsidc/dev-vm-search/src/661fb940003356c042e5f877dc97abe48640241c/Vagrantfile?at=master))

## Design

This is essentially just a fork of the base swagger-ui project on github. We
added a node express server to host the static assets, and we are changing the
service endpoint via the jenkins_provision script that is defined in the
documentation-index-vm project.

### How to contact NSIDC ###

User Services and general information:  
Support: http://support.nsidc.org  
Email: nsidc@nsidc.org  

Phone: +1 303.492.6199  
Fax: +1 303.492.2468  

Mailing address:  
National Snow and Ice Data Center  
CIRES, 449 UCB  
University of Colorado  
Boulder, CO 80309-0449 USA  

### License ###

Every file in this repository is covered by the Apache License, Version 2.0; a
copy of the license is included in the file COPYING.
