var connectSSI   = require('connect-ssi');
var errorhandler = require('errorhandler');
var path         = require('path');
var proxy        = require('proxy-middleware');
var url          = require('url');

var configureProxies = function (app) {
  if (app.get('env') === 'development') {
    app.use('/images', proxy(url.parse('http://qa.nsidc.org/images')));
  }
};

var configureSSI = function (app) {
  var env = app.get('env'),
      ssiHost = 'http://nsidc.org';

  if (env === 'development') {
    ssiHost = 'http://qa.nsidc.org';
  } else if (env !== 'production') {
    ssiHost = 'http://' + env + '.nsidc.org';
  }

  app.use(connectSSI({
    root: 'index.html',
    host: ssiHost
  }));
};

module.exports = function(app, express) {
  var appDir = path.join(__dirname, '..', 'dist');

  process.chdir(appDir);

  app.use(errorhandler());

  configureSSI(app);

  app.use(express.static(appDir));

  configureProxies(app);
};
