casper.test.begin('NSIDC Web Services', function suite(test) {
  var env = casper.cli.get('node-env'),
      appUrl = 'web-services-docs.apps.int.nsidc.org/',
      url = 'http://localhost:8081';

  if (env === 'production') {
    url = 'http://' + appUrl;
  } else if (env === 'integration' ||
      env === 'qa' ||
      env === 'staging') {
    url ='http://' + env + '.' + appUrl;
  }

  // connect to the page
  casper.start(url);
  casper.test.comment('Running CasperJS tests against ' + url);

  // run some basic rendering tests
  casper.then(function () {
    test.assertTitle('NSIDC Web Services',
                     '<title> is "NSIDC Web Services"');

    test.assertSelectorHasText('.info_title',
                               'NSIDC Web Service Documentation Index',
                               'title is displayed on the page');

    test.assertElementCount('ul#resources', 1,
                            'renders the container for web service resources');

    test.assertExists('#resources li .heading h2 a',
                      'renders a heading for at least one service');

    test.assertElementCount('.info_contact a', 1,
                            'includes a mailto link to contact the developer');

    test.assertElementCount('#header', 1,
                            'includes the NSIDC SSIs');
  });

  // complete and close the test suite
  casper.run(function () {
    test.done();
  });
});
