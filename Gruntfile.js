module.exports = function(grunt){
  grunt.initConfig({
    build_num: process.env.BUILD_NUMBER,
    version_log: process.env.ARTIFACT_VERSION_LOG,
    pkg: grunt.file.readJSON('package.json'),
    full_version: '<%= pkg.version %>+build.<%= build_num %>',
    tar_file: 'documentation-index-ui-<%= full_version %>.tar',

    copy: {
      dist: {
        files: [{
          expand: true,
          src: [
            '**/*'
          ],
          dest: 'dist/webapps/'
        }]
      },
      package_and_repo_distribute: {
        files: [{
          expand: true,
          src: ['documentation-index-ui-<%= pkg.version %>+build.<%= build_num %>.tar'],
          dest: '/disks/integration/san/INTRANET/REPO/documentation-index-ui'
        }]
      }
    },
    shell: {
      build: {
        command: 'npm run-script build',
        options: {
          stdin: false
        }
      }
    },
    compress: {
      package_and_repo_distribute: {
        options: {
          mode: 'tar',
          archive: '<%= tar_file %>'
        },
        expand: true,
        src: ['./*', './dist/**/*', './config/**/*'],
        dest: './'
      }
    },

    casperjs: {
      options: {
        casperjsOptions: [
          '--node-env=' + process.env.NODE_ENV,
          grunt.option('no-color') ? ['--no-colors'] : '--colors'
        ]
      },
      files: ['test/casperjs/*_test.js']
    }

  });

  grunt.registerTask('add_version_to_log', 'Add the build version to the log',
    function(version_arg){
      var version_file = grunt.config('version_log');
      var version = grunt.config('full_version');

      if (arguments.length === 1) {
        version = version_arg;
      }

      if (!grunt.file.exists(version_file)) {
        grunt.file.write(version_file, 'buildVersion=' + version);
        grunt.log.writeln('Created version log ' + version_file);
      }
      else {
        grunt.file.write(version_file, 'buildVersion=' + version);
        grunt.log.writeln('Updated version log ' + version_file);
      }
    }
  );

  grunt.registerTask('write_version_properties', 'Add the build version to a local properties file',
    function(version_arg){
      var version = grunt.config('full_version');
      if (arguments.length === 1) {
        version = version_arg;
      }

      grunt.file.write('version.properties', 'ARTIFACT_VERSION=', version);
      grunt.log.writeln('Created version.properties with ARTIFACT_VERSION=', version);
    }
  );

  grunt.registerTask('build', ['shell:build']);
  grunt.registerTask('dist', ['build', 'compress', 'copy']);
  grunt.registerTask('package_and_repo_distribute', ['dist', 'add_version_to_log']);

  grunt.loadNpmTasks('grunt-shell');
  grunt.loadNpmTasks('grunt-casperjs');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-compress');
}
