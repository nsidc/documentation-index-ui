var config = require( './config/express-config.js');
var express = require('express');

var app = express();

config(app, express);

app.listen(8081, function(){
  console.log('listening at 8081, 0.0.0.0');
});
